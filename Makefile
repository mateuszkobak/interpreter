all:
	ghc Interpreter.hs -o interpreter

clean:
	-rm -f *.log *.aux *.hi *.o *.dvi interpreter
