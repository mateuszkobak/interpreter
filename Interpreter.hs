module Main where

import System.IO ( stdin, hGetContents )
import System.Environment ( getArgs, getProgName )
import System.Exit ( exitFailure, exitSuccess )
import Control.Monad (when)
import qualified Data.Map as Map

import Control.Monad.Reader (ReaderT, ask, local, runReaderT)
import Control.Monad.State (StateT, get, put, runStateT)
import Control.Monad.Except (ExceptT, runExceptT)

import LexGrammar
import ParGrammar
import SkelGrammar
import PrintGrammar
import AbsGrammar
import ErrM

import Utils
import Lang

myLLexer = myLexer

type Verbosity = Int

-- Execute parsed program
executeProgram :: Verbosity -> Program PP -> IO ()
executeProgram v (Program _ stmts) = do
    let initStore = (Map.empty, 0) 
    let initEnv = Map.empty
    err <- runExceptT (runStateT (runReaderT (execBlock stmts) initEnv) initStore)
    case err of
      Left err -> do 
        putStrLn $ show err
        exitFailure
      Right _ -> do 
        putStrV v "\nProgram executed with no error!\n"
        exitSuccess
    return ()

-- Prints or not according to given verbosity
putStrV :: Verbosity -> String -> IO ()
putStrV v s = when (v > 1) $ putStrLn s

-- Reads program from file and runs it
runFile :: Verbosity -> FilePath -> IO ()
runFile v f = readFile f >>= run v

-- Runs program from given string argument
run :: Verbosity -> String -> IO()
run v s = let ts = myLLexer s in 
    case pProgram ts of
        Bad s    -> do 
            putStrLn "\nERROR: Parse Failed!\n"
            putStrV v "Tokens:"
            putStrV v $ show ts
            putStrLn s
            exitFailure
        Ok  tree -> do 
            putStrV v "\nParse Successful!"
            showTree v tree
            putStrV v "\n[Execution output]\n\n"
            executeProgram v tree

-- Shows parsed tree
showTree :: (Show a, Print a) => Int -> a -> IO ()
showTree v tree = do
    putStrV v $ "\n[Abstract Syntax]\n\n" ++ show tree
    putStrV v $ "\n[Linearized tree]\n\n" ++ printTree tree

-- Displays interpreter's usage
usage :: IO ()
usage = do
    putStrLn $ unlines
        [ "usage: Call with one of the following argument combinations:"
        , "  --help          Display this help message."
        , "  (no arguments)  Parse stdin."
        , "  (files)         Parse content of files."
        , "  -s (files)      Parse content of files in verbosed mode."
        ]
    exitFailure

main :: IO ()
main = do
    args <- getArgs
    case args of
        ["--help"] -> usage
        [] -> getContents >>= run 0
        ("-v":fs) -> mapM_ (runFile 2) fs
        fs -> mapM_ (runFile 0) fs