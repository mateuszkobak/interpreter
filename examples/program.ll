int a = 7;

int fun(int x) {
    if (x > 47) {
        return x;
    }
    else {
        return fun(a + x);
    }
}

void costam(int& a) {
    a = fun(a);
}


int x = 32;
costam(x);
print("Wynik to: " + intToString(x)); // powinno wypisać 53

x = 10;

void dawaj(int &x) {
    int i = 0;
    while (true) {
        x = x + 2;
        i++;
        if (x > 23) return;
    }
}

dawaj(x);
print("Teraz wynik to: " + intToString(x)); // powinno wypisać 24

