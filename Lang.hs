module Lang where

import System.IO ( stdin, hGetContents )
import System.Environment ( getArgs, getProgName )
import System.Exit ( exitFailure, exitSuccess )

import Control.Monad.Reader
import Control.Monad.State
import Control.Monad.Except

import AbsGrammar

import Utils

-- Creates environment for invoking function
-- Adds argument with proper values to the environment
-- Returns Nothing if the number of arguments is wrong
createFunEnv :: [Arg PP] -> [Expr PP] -> Env -> Inst (Maybe Env)
createFunEnv ((VArg _ _ id):as) (exp:es) funEnv = do
    funEnv <- local (const funEnv) $ declareVar id
    Just val <- evalExpr exp
    local (const funEnv) $ setValue id val Nothing
    mFunEnv <- createFunEnv as es funEnv
    return mFunEnv
createFunEnv ((RefArg _ _ argId):as) ((EVar _ varId):es) funEnv = do
    funEnv <- setReference argId varId funEnv
    mFunEnv <- createFunEnv as es funEnv
    return mFunEnv
createFunEnv [] (x:xs) _ = return Nothing
createFunEnv (x:xs) [] _ = return Nothing
createFunEnv [] [] funEnv = return (Just funEnv)

-- Function returning a monad evaluating given expression
evalExpr :: Expr PP -> Inst ResultVal

evalExpr(EVar info id) = getValue id info

evalExpr(ELitInt info n) = 
    return (Just (IntVal n))

evalExpr(ELitTrue info) =
    return (Just (BoolVal True))

evalExpr(ELitFalse info) =
    return (Just (BoolVal False))

-- Execute built-in print function
evalExpr(EApp info (Ident "print") (x: xs)) = do
    Just (StringVal s) <- evalExpr x
    myPrint s
    evalExpr (EApp info (Ident "print") xs)
    return Nothing
evalExpr(EApp _ (Ident "print") []) = do
    return Nothing

-- Apply built-in function converting int to string
evalExpr(EApp _ (Ident "intToString") (x: [])) = do
    Just (IntVal i) <- evalExpr x
    return (Just (StringVal (show i)))
evalExpr(EApp info (Ident "intToString") _) = do
    throwError (Error "wrong number of arguments in function application" info)

evalExpr(EApp info id arg_exps) = do
    Just (FunVal (block, funEnv, args, _)) <- getValue id info
    mFunEnv <- createFunEnv args arg_exps funEnv
    case mFunEnv of
        Just funEnv -> do
            (_, resVal) <- local (const funEnv) $ execBlock block
            return resVal
        Nothing -> throwError (Error "wrong number of arguments in function application" info)

evalExpr(EString info s) =
    return (Just (StringVal (prepareString 0 [] s)))

evalExpr(Neg info e) = do
    Just(IntVal  i) <- evalExpr e
    return (Just (IntVal (-i)))

evalExpr(Not info e) = do
    Just (BoolVal b) <- evalExpr e
    return (Just (BoolVal (not b)))

evalExpr(EMul info e1 (Div info2) e2) = do
    Just (IntVal m) <- evalExpr e2
    if m == 0 then
        throwError (Error "division by zero" info)
    else
        liftM2 (getMulOp (Div info2)) (evalExpr e1) (return (Just (IntVal m)))
evalExpr(EMul info e1 (Mod info2) e2) = do
    Just (IntVal m) <- evalExpr e2
    if m == 0 then
        throwError (Error "zero modulus" info)
    else
        liftM2 (getMulOp (Mod info2)) (evalExpr e1) (return (Just (IntVal m)))
evalExpr(EMul info e1 mOp e2) =
    liftM2 (getMulOp mOp) (evalExpr e1) (evalExpr e2)

evalExpr(EAdd info e1 aOp e2) = do
    v1 <- evalExpr e1
    v2 <- evalExpr e2
    case (v1, v2) of
        ((Just (IntVal a)),(Just (IntVal b))) -> liftM2 (getAddOp aOp) (return v1) (return v2)
        ((Just (StringVal a)), (Just (StringVal b))) -> liftM2 (getAddOp aOp) (return v1) (return v2)
        _ -> throwError (Error "bad types for binary operation" info)

evalExpr(ERel info e1 rOp e2) = 
    liftM2 (getRelOp rOp) (evalExpr e1) (evalExpr e2)

evalExpr(EAnd info e1 e2) = 
    liftM2 getAndOp (evalExpr e1) (evalExpr e2)

evalExpr(EOr info e1 e2) = 
    liftM2 getOrOp (evalExpr e1) (evalExpr e2)


-- Function returns monad executing given statement.
-- Monad return environment, because a statement can change it,
-- and ResultVal, because we wanty to keep track of "return" statements
execStmt :: Stmt PP -> Inst (Env, ResultVal)

execStmt (Empty _) = do
    env <- ask
    return (env, Nothing)

execStmt (BStmt _ (Block _ stmts)) = execBlock stmts

execStmt (Decl info t (x:xs)) =   
    case x of
        NoInit _ id -> do
            new_env <- declareVar id
            (new_env, ret) <- local (const new_env) $ execStmt (Decl info t xs)
            return (new_env, ret)
        Init info id expr -> do
            new_env <- declareVar id
            Just val <- evalExpr expr
            local (const new_env) $ setValue id val info
            (new_env, ret) <- local (const new_env) $ execStmt (Decl info t xs)
            return (new_env, ret)            
execStmt (Decl info t []) = do
    env <- ask
    return (env, Nothing)

execStmt (Ass info id expr) = do
    Just val <- evalExpr expr
    setValue id val info
    returnEnv

execStmt (Incr info id) = do
    Just (IntVal i) <- getValue id info
    setValue id (IntVal (i + 1)) info
    returnEnv

execStmt (Decr info id) = do
    Just (IntVal i) <- getValue id info
    setValue id (IntVal (i - 1)) info
    returnEnv

execStmt (Ret _ e) = do
    resVal <- evalExpr e
    env <- ask
    return (env, resVal)

execStmt (VRet _) = do
    env <- ask
    return (env, Just VoidVal)
    
execStmt (Cond info e stmt) = do
    execStmt (CondElse info e stmt (Empty Nothing))

execStmt (CondElse _ e s1 s2) = do
    Just (BoolVal b) <- evalExpr e
    env <- ask
    if b then do
        (_, ret) <- execStmt s1
        return (env, ret)
    else do
        (_, ret) <- execStmt s2
        return (env, ret)

execStmt (While info e stmt) = do
    Just (BoolVal b) <- evalExpr e
    env <- ask
    if b then do
        (_, ret) <- execStmt stmt
        case ret of
            Just val -> return (env, Just val)
            Nothing -> execStmt (While info e stmt)
    else
        returnEnv

execStmt (SExp _ e) = do
    evalExpr e
    returnEnv

execStmt (FnDef info t id args (Block _ stmts)) = do
    newEnv <- declareVar id
    local (const newEnv) $ setValue id (FunVal (stmts, newEnv, args, t)) info
    return (newEnv, Nothing)

execBlock :: [Stmt PP] -> Inst (Env, ResultVal)
execBlock [] = returnEnv
execBlock (x:xs) = do
    (env, ret) <- execStmt x
    case ret of
        Nothing -> local (const env) (execBlock xs)
        Just _ -> return (env, ret)




