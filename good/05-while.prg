// Program oblicza sumę liczb od 0 do 10
// Widzimy zastosowanie pętli while i inkrementacji intów

int i = 0, sum = 0;
while (i <= 10) {
    sum = sum + i;
    i++;
}

print(intToString(sum));