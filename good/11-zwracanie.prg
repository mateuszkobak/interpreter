string fun(string a, string b) {
    return a + ", " + b;
}

boolean xor(boolean a, boolean b) {
    return (!a && b) || (a && !b);
}

if (xor(true, false)) {
    print("ok");
}

print(fun("jeden", "dwa"));