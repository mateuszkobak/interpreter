void check(int idx, boolean b) {
    if (!b) print("Blad: " + intToString(idx));
}

check(0, 0 == 1); // tylko tutaj powinien być błąd

check(1000, 15 == 15);

check(1, 2 + 3 == 5);

check(2, 2 * 3 == 6);

check(3, 10 % 3 == 1);

check(4, 10 / 3 == 3);

check(5, 10 > 7);

check(6, 10 >= 7);

check(7, 10 >= 10);

check(8, 10 < 17);

check(9, 10 <= 17);

check(10, 10 <= 10);

check(11, 10 != 17);