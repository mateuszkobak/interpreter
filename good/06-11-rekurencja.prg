// Policzymy silnię rekurencyjnie

int silnia(int n) {
    if (n == 1) return 1; // tutaj też widać działanie operacji "return"
    return n * silnia(n - 1);
}

print(intToString(silnia(6)));