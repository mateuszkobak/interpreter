int fun (int a) {
    a = a + 10;
    return a;
}

int b = 5;
int c = fun(b);
print("b: " + intToString(b) + " c: " + intToString(c));

// b powinno pozostać równe 5
// zaś c to wynik zwrócony przez fun(5), czyli 15