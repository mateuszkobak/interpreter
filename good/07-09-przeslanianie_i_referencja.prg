int c = 7;
void addc(int &x) { // zmienna podana przez referencję
    x = x + c;
    return;
}

int fun(int a) {
    int c = 15; // to c przysłania to poprzednie
    addc(a); // ale ta funkcja doda do a liczbę 7
    return a + c; // więc wynik to a + 22
}

print(intToString(fun(-10))); // powinno wypisać 12