module Utils where

import AbsGrammar
import qualified Data.Map as Map
import Control.Monad.Reader 
import Control.Monad.State 
import Control.Monad.Except 

-- Type used for passing (line, col) to prompt in case of runtime exception
type PP = Maybe (Int, Int)

-- Memory is indexed by nonnegative integers of this type
type Loc = Integer

-- Environment maps identifiers to cells in memory
type Env = Map.Map Ident Loc

-- Stores values in memory cells
type MyStore = Map.Map Loc Val

-- State consists of the MyStore and the index of next free cell in memory
type MyState  = (MyStore, Loc)

-- Types of values stored in MyStore
data Val = 
      StringVal String 
    | IntVal Integer 
    | BoolVal Bool 
    | FunVal Func 
    | VoidVal
    deriving (Show)

-- Function type consists of (function body, environment in which function runs, arguments declarations, type of returned value)
type Func = ([Stmt PP], Env, [Arg PP], Type PP) 

-- This type helps in recognizing whether there already was a "return" statement
type ResultVal = Maybe Val

-- Error type returned by interpreter if there was a runtime exception
data Error = Error String PP
instance Show Error where
    show (Error msg (Just (line, column))) = 
        ((showString "ERROR!\nRuntime exception: ") . (showString msg) . 
        (showString ",\noccured in line ") . (shows line) . 
        (showString " in column ") . (shows column) .
        (showString ".")) ""
    show (Error msg Nothing) =
        ((showString "ERROR!\nRuntime exception: ") . (showString msg)) ""   

-- Our fabulous monad representing instructions in the program
type Inst a = ReaderT Env (StateT MyState (ExceptT Error IO)) a

-- Declares new variable with given id
declareVar :: Ident -> Inst Env
declareVar id = do
    (state, next_loc) <- get
    put (state, next_loc + 1)
    asks (Map.insert id next_loc)

-- Sets value to variable with already declared id
setValue :: Ident -> Val -> PP -> Inst ()
setValue id val info = do
    mLoc <- asks (Map.lookup id)
    case mLoc of
        Just loc -> modify (\(state, next_loc) -> (Map.insert loc val state, next_loc)) 
        Nothing -> throwError (Error ("variable " ++ (showIdent id) ++ " not declared") info)

-- Get value of variable with given id
getValue :: Ident -> PP -> Inst ResultVal
getValue id info = do
    mLoc <- asks $ Map.lookup id
    case mLoc of
        Just loc -> gets $ (Map.lookup loc) . fst
        Nothing -> throwError (Error ("variable " ++ (showIdent id) ++ " not declared") info)

-- Adds argId to funEnv. Id argId refers to the same cell in memory as varId.
setReference :: Ident -> Ident -> Env -> Inst Env
setReference argId varId funEnv = do
    Just loc <- asks (Map.lookup varId)
    return (Map.insert argId loc funEnv)

-- Lifted print operation
myPrint :: String -> Inst ()
myPrint str = do
    (lift . lift . lift) $ putStrLn str

-- Here come all the unary and binary operations
-- Assigns parsed operators the coresponding functions
getMulOp :: MulOp PP -> (ResultVal-> ResultVal -> ResultVal)
getMulOp op (Just (IntVal a)) (Just (IntVal b)) = 
    case op of
        Times _ -> Just (IntVal (a * b))
        Div _ -> Just (IntVal (a `div` b))
        Mod _ -> Just (IntVal (a `mod` b))
getAddOp :: AddOp PP -> (ResultVal-> ResultVal -> ResultVal)
getAddOp op (Just (IntVal a)) (Just (IntVal b)) = 
    case op of
        Plus _ -> Just (IntVal (a + b))
        Minus _ -> Just (IntVal (a - b))
getAddOp (Plus _)  (Just (StringVal a)) (Just (StringVal b)) =
    Just (StringVal (a ++ b)) 
getRelOp :: RelOp PP -> (ResultVal-> ResultVal -> ResultVal)
getRelOp op (Just (IntVal a)) (Just (IntVal b)) = 
    case op of
        LTH _ -> Just (BoolVal (a < b))
        LE _ -> Just (BoolVal (a <= b))
        GTH _ -> Just (BoolVal (a > b))
        GE _ -> Just (BoolVal (a >= b))
        EQU _ -> Just (BoolVal (a == b))
        NE _ -> Just (BoolVal (a /= b))
getAndOp :: (ResultVal-> ResultVal -> ResultVal)
getAndOp (Just (BoolVal b1)) (Just (BoolVal b2)) =
    Just (BoolVal (b1 && b2))
getOrOp :: (ResultVal-> ResultVal -> ResultVal)
getOrOp (Just (BoolVal b1)) (Just (BoolVal b2)) =
    Just (BoolVal (b1 || b2))

-- Helps being concise
returnEnv :: Inst (Env, ResultVal)
returnEnv = do
    env <- ask
    return (env, Nothing)

-- Cuts citation signs from string
prepareString :: Integer -> String -> String -> String
prepareString 0 [] (s:ss) = prepareString 1 [] ss
prepareString 1 out (s1:[]) = out
prepareString 1 out (s1:ss) = prepareString 1 (out++[s1]) ss

-- Helper for showing undeclared identifier error
showIdent :: Ident -> String
showIdent (Ident str) = show str