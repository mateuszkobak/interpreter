# Interpreter na zajęcia JPP
Interpreter języka opartego na języku Latte. Pliki `Intepreter.hs`, `Lang.hs` oraz `Utils.hs` zawierają implementację interpretera. Reszta blików została wygenerowana przez generator parserów BNFC.

# Uruchamianie
Interpreter należy uruchomić za pomocą polecenia
```
$ ./interpreter program_file
```
gdzie `program_file` jest plikiem z kodem programu. Opcja `-v` umożliwia działanie progrmu z wypisywaniem szczegółowych informacji o parsowaniu.

# Sposób implementacji
W mojej implementacji kluczową rolę gra monada reprezentująca obliczenia w moim języku
```
type Inst a = ReaderT Env (StateT MyState (ExceptT Error IO)) a
```
gdzie wykorzytywane są transoformatory monad `ReaderT`, `StateT` oraz `ExceptT` zastosowane na monadzie `IO`. Teraz omówię argumenty użyte w tych konstruktorach.

#### Obsługa błędów
```
type PP = Maybe (Int, Int)
data Error = Error String PP
```
Typ `PP` odpowiada za przechowywanie informacji o linii i kolumnie w kodzie, która jest obecnie interpretowana. W razie błędu wykonania wypisywany jest komunikat (stąd element typu `String`) oraz miejsce w kodzie, w którym wystąpił.

#### Stan
```
type Loc = Integer
type MyStore = Map.Map Loc Val
type MyState  = (MyStore, Loc)
```
Stan to krotka typu `(MyStore, Loc)`, gdzie pierwszy element to `Map.Map` przyporządkowujący indeksowi "komórki w pamięci" jej obecną wartość, zaś drugi element, to indeks następnej wolnej komórki.

#### Środowisko
```
type Env = Map.Map Ident Loc
```
Środowisko to mapa przypisująca identyfikatorowi odpowiednią komórkę w pamięci.
# Komunikaty o błędach
Interpreter informuje użytkownika o błędach wykonania oraz o błędach parsowania. Wypisuje rodzaj błędu i miejsce wystąpienia w kodzie.